-- luacheck: globals widget volume_now mem_now coretemp_now cpu_now net_now weather_now

local beautiful = require("beautiful")
local dpi = require("beautiful.xresources").apply_dpi
local lain = require("lain")
local wibox = require("wibox")

local markup = lain.util.markup

-- [[ ALSA volume
local volicon = wibox.widget.imagebox(beautiful.widget_volume)
beautiful.volume = lain.widget.alsa({
  --togglechannel = "IEC958,3",
  settings = function()
    local level  = volume_now.level

    if volume_now.status == "off" then
      level = level .. "% M"
    else
      level = level .. "%"
    end

    widget:set_markup(markup.font(beautiful.font, level))
  end
})
local volume = wibox.container.margin(wibox.widget { volicon, beautiful.volume.widget, layout = wibox.layout.align.horizontal }, dpi(2), dpi(5))
--]]

-- [[ Calendar
local clock = wibox.container.margin(wibox.widget.textclock(), dpi(2), dpi(5))
beautiful.cal = lain.widget.cal({
  --cal = "cal --color=always",
  attach_to = { clock },
  notification_preset = {
    font = beautiful.monespaced or 'Monospace 8',
    fg = beautiful.fg_normal,
    bg = beautiful.bg_normal
  }
})
--]]

-- [[ MEM
local memicon = wibox.widget.imagebox(beautiful.widget_mem)
local mem = lain.widget.mem({
  settings = function()
    local mem_used = math.floor((mem_now.used / 1024) * 100) / 100
    widget:set_markup(markup.font(beautiful.font, " " .. mem_used .. "GB"))
  end
})
local memory = wibox.container.margin(wibox.widget { memicon, mem.widget, layout = wibox.layout.align.horizontal }, dpi(2), dpi(5))
--]]

-- [[ Coretemp (lain, average)
local temp = lain.widget.temp({
  settings = function()
    widget:set_markup(markup.font(beautiful.font, " " .. coretemp_now .. "°C"))
  end
})
local tempicon = wibox.widget.imagebox(beautiful.widget_temp)
local temperature = wibox.container.margin(wibox.widget { tempicon, temp.widget, layout = wibox.layout.align.horizontal }, dpi(2), dpi(5))
--]]

-- [[ CPU
local cpuicon = wibox.widget.imagebox(beautiful.widget_cpu)
local cpu = lain.widget.cpu({
  settings = function()
    widget:set_markup(markup.font(beautiful.font, " " .. cpu_now.usage .. "%"))
  end
})
local processor = wibox.container.margin(wibox.widget { cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, dpi(3), dpi(5))
--]]

-- [[ Net
local neticon = wibox.widget.imagebox(beautiful.widget_net)
local net = lain.widget.net({
  settings = function()
    widget:set_markup(markup.fontfg(beautiful.font, "#FEFEFE", net_now.received .. " ↓↑ " .. net_now.sent))
  end
})
local network = wibox.container.margin(wibox.widget { neticon, net.widget, layout = wibox.layout.align.horizontal }, dpi(3), dpi(5))
--]]

-- [[ Taskwarrior
local task = wibox.widget.imagebox(beautiful.widget_task)
lain.widget.contrib.task.attach(task, {
    -- do not colorize output
    show_cmd = "task | sed -r 's/\\x1B\\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g'"
})
--]]

-- [[ Weather
beautiful.weather = lain.widget.weather({
  city_id = 2643743,
  settings = function()
    local units = math.floor(weather_now["main"]["temp"])
    widget:set_markup(" " .. units .. "°C")
  end
})

local weather = wibox.container.margin(
  wibox.widget { beautiful.weather.icon, beautiful.weather.widget, layout = wibox.layout.align.horizontal },
  dpi(3),
  dpi(5)
)
--]]

return {
  clock = clock,
  memory = memory,
  network = network,
  cpu = processor,
  task = task,
  temperature = temperature,
  weather = weather,
  volume = volume
}
