local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup").widget


local key = awful.key
local modkey = awful.util.metakeys.mod
local altkey = awful.util.metakeys.alt
local ctrlkey = awful.util.metakeys.ctrl
local shiftkey = awful.util.metakeys.shift

-- local music = "spotify"
local mymainmenu = awful.util.mymainmenu
local menubar = awful.util.menubar
local launcher = awful.util.apps.launcher
local volume = require('lib.media-controls').volume

-- {{{ Global Key bindings
local globalkeys = gears.table.join(
  key({modkey}, "s", hotkeys_popup.show_help, {description = "show help", group = "awesome"}),
  key({modkey}, "Tab", awful.tag.viewnext, {description = "view next", group = "tag"}),
  key({modkey, shiftkey}, "Tab", awful.tag.viewprev, {description = "view previous", group = "tag"}),
  key({modkey}, "Escape", awful.tag.history.restore, {description = "go back", group = "tag"}),
  key({modkey}, "w", function() mymainmenu:show() end, {description = "show main menu", group = "awesome"}),
  -- Default client focus
  key({altkey}, "Tab", function () awful.client.focus.byidx( 1) end, {description = "focus next by index", group = "client"}),
  key({altkey, shiftkey}, "Tab", function () awful.client.focus.byidx(-1) end, {description = "focus previous by index", group = "client"}),

  -- Layout manipulation
  key({modkey, shiftkey}, "j", function() awful.client.swap.byidx(1) end, {description = "swap with next client by index", group = "client"}),
  key({modkey, shiftkey}, "k", function() awful.client.swap.byidx(-1) end, {description = "swap with previous client by index", group = "client"}),
  key({modkey, ctrlkey}, "j", function() awful.screen.focus_relative(1) end, {description = "focus the next screen", group = "screen"}),
  key({modkey, ctrlkey}, "k", function() awful.screen.focus_relative(-1) end, {description = "focus the previous screen", group = "screen"}),
  key({modkey}, "u", awful.client.urgent.jumpto, {description = "jump to urgent client", group = "client"}),

  -- Standard program
  key({modkey}, "Return", function() awful.spawn(awful.util.apps.terminal) end, {description = "open a terminal", group = "launcher"}),
  key({modkey, ctrlkey}, "r", awesome.restart, {description = "reload awesome", group = "awesome"}),
  key({modkey, ctrlkey}, "q", awesome.quit, {description = "quit awesome", group = "awesome"}),
  key({modkey}, "l", function() awful.tag.incmwfact(0.05) end, {description = "increase master width factor", group = "layout"}),
  key({modkey}, "h", function() awful.tag.incmwfact(-0.05) end, {description = "decrease master width factor", group = "layout"}),
  key({modkey, shiftkey}, "h", function() awful.tag.incnmaster(1, nil, true) end, {description = "increase master clients", group = "layout"}),
  key({modkey, shiftkey}, "l", function() awful.tag.incnmaster(-1, nil, true) end, {description = "decrease master clients", group = "layout"}),
  key({modkey, ctrlkey}, "h", function() awful.tag.incncol(1, nil, true) end, {description = "increase column count", group = "layout"}),
  key({modkey, ctrlkey}, "l", function() awful.tag.incncol(-1, nil, true) end, {description = "decrease column count", group = "layout"}),
  key({modkey, ctrlkey}, "n", function()
      local c = awful.client.restore()
      if c then
        c:emit_signal("request::activate", "key.unminimize", {raise = true})
      end
    end,
    {description = "restore minimized", group = "client"}
  ),

  -- Launcher
  key({modkey}, "space", function() os.execute(launcher) end, {description = "show system wide search and launcher", group = "launcher"}),
  -- Menubar
  key({modkey}, "p", function() menubar.show() end, {description = "show the menubar", group = "launcher"}),
  -- Volume control
  key({ altkey }, ".", volume.up, {description = "volume up", group = "hotkeys"}),
  key({ }, "XF86AudioRaiseVolume", volume.up, {description = "volume up", group = "hotkeys"}),
  key({ altkey }, ",", volume.down, {description = "volume down", group = "hotkeys"}),
  key({ }, "XF86AudioLowerVolume", volume.down, {description = "volume down", group = "hotkeys"}),
  key({ altkey }, "m", volume.mute, {description = "toggle mute", group = "hotkeys"}),
  key({ }, "XF86AudioMute", volume.mute, {description = "toggle mute", group = "hotkeys"}),
  key({ altkey, ctrlkey }, "m", volume.max, {description = "volume 100%", group = "hotkeys"}),
  key({ altkey, ctrlkey }, "0", volume.min, {description = "volume 0%", group = "hotkeys"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  globalkeys = gears.table.join(globalkeys, -- View tag only.
    key({modkey}, "#" .. i + 9, function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then tag:view_only() end
      end,
      {description = "view tag #" .. i, group = "tag"}
    ),
    -- Toggle tag display.
    key({modkey, ctrlkey}, "#" .. i + 9, function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then awful.tag.viewtoggle(tag) end
      end,
      {description = "toggle tag #" .. i, group = "tag"}
    ),
    -- Move client to tag.
    key({modkey, shiftkey}, "#" .. i + 9, function()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then client.focus:move_to_tag(tag) end
        end
      end,
      {description = "move focused client to tag #" .. i, group = "tag"}
    ),
    -- Toggle tag on focused client.
    key({modkey, ctrlkey, shiftkey}, "#" .. i + 9, function()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then client.focus:toggle_tag(tag) end
        end
      end,
      {description = "toggle focused client on tag #" .. i, group = "tag"}
    )
  )
end
-- }}}

-- {{{ Client keys
local clientkeys = gears.table.join(
  key({modkey}, "f", function(c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end,
    {description = "toggle fullscreen", group = "client"}
  ),
  key({modkey, shiftkey}, "c", function(c) c:kill() end, {description = "close", group = "client"}),
  key({modkey, ctrlkey}, "space", awful.client.floating.toggle, {description = "toggle floating", group = "client"}),
  key({modkey, ctrlkey}, "Return", function(c) c:swap(awful.client.getmaster()) end, {description = "move to master", group = "client"}),
  key({modkey}, "o", function(c) c:move_to_screen() end, { description = "move to screen", group = "client"}),
  key({modkey}, "t", function(c) c.ontop = not c.ontop end, {description = "toggle keep on top", group = "client"}),
  -- The client currently has the input focus, so it cannot be
  -- minimized, since minimized clients can't have the focus.
  key({modkey}, "n", function(c)
      c.minimized = true
    end,
    {description = "minimize", group = "client"}
  ),
  key({modkey}, "m", function(c)
      c.maximized = not c.maximized
      c:raise()
    end,
    {description = "(un)maximize", group = "client"}
  ),
  key({modkey, ctrlkey}, "m", function(c)
      c.maximized_vertical = not c.maximized_vertical
      c:raise()
    end,
    {description = "(un)maximize vertically", group = "client"}
  ),
  key({modkey, shiftkey}, "m", function(c)
      c.maximized_horizontal = not c.maximized_horizontal
      c:raise()
    end,
    {description = "(un)maximize horizontally", group = "client"}
  )
)
--- }}}

return {
  client = clientkeys,
  global = globalkeys
}
