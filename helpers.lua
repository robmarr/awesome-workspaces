local awful = require("awful")

-- {{ execute programs
local function exec_wrapped(program)
  return function () os.execute(program) end
end

local function spawn(program, shell)
  if shell == true then
    return function () awful.spawn.with_shell(program) end
  else
    return function () awful.spawn(program) end
  end
end

local function run_once(cmd_arr)
  for _, cmd in ipairs(cmd_arr) do
    awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
  end
end

return {
  exec = exec_wrapped,
  run_once = run_once,
  spawn = spawn
}
-- }}
