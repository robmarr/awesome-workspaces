local awful = require("awful")
local lain = require("lain")

-- client control
local function fullscreen_toggle(c)
  c.fullscreen = not c.fullscreen
  c:raise()
end

local function clientshift(idx)
  return function () awful.client.swap.byidx(idx) end
end

local function clientshift_focus(idx)
  return function () awful.client.focus.byidx(idx) end
end

local function client_focus_bydirection(dir)
  return function()
    awful.client.focus.global_bydirection(dir)
    if client.focus then client.focus:raise() end
  end
end

local function client_maximise(c)
  c.maximized = not c.maximized
  c:raise()
end

local function client_minimise(c)
  c.minimized = true
end

local function client_toggle_on_top(c)
  c.ontop = not c.ontop
end

local function client_to_master(c)
  c:swap(awful.client.getmaster())
end

local function client_move_screen(c)
  c:move_to_screen()
end

local function client_close(c)
  c:kill()
end

awful.tag.viewnonempty = function (idx)
  lain.util.tag_view_nonempty(idx)
end

return {
  fullscreen_toggle = fullscreen_toggle,
  clientshift = clientshift,
  clientshift_focus = clientshift_focus,
  client_focus_bydirection = client_focus_bydirection,
  client_maximise = client_maximise,
  client_minimise = client_minimise,
  client_toggle_on_top = client_toggle_on_top,
  client_to_master = client_to_master,
  client_move_screen = client_move_screen,
  client_close = client_close
}
