-- wibox control
local function wibox_toggle()
  for s in screen do
    s.mywibox.visible = not s.mywibox.visible
    if s.mybottomwibox then
        s.mybottomwibox.visible = not s.mybottomwibox.visible
    end
  end
end

return {
  wibox_toggle = wibox_toggle
}
