local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

local bars = require("lib.bars")

local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then wallpaper = wallpaper(s) end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end

local function create(s)
  -- Wallpaper
  set_wallpaper(s)

  -- Each screen has its own tag table.
  awful.tag(awful.util.tagnames, s, awful.util.layout)

  -- Topbar
  bars.top(s)

  -- Bottombar
  bars.bottom(s)

end

return  {
  create = create,
  set_wallpaper = set_wallpaper
}
