-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local lain = require("lain")
local wibox = require("wibox")

local arrow = lain.util.separators.arrow_left
local widgets = awful.util.widgets

local modkey = awful.util.metakeys.mod

-- {{{ Topbar
-- Create a wibox for each screen

-- Layout selector
local function layouts_selector(s)
  s.layouts_selector = awful.widget.layoutbox(s)
  s.layouts_selector:buttons(gears.table.join(
    awful.button({}, 1, function() awful.layout.inc(1) end),
    awful.button({}, 3, function() awful.layout.inc(-1) end),
    awful.button({}, 4, function() awful.layout.inc(1) end),
    awful.button({}, 5, function() awful.layout.inc(-1) end)
  ))
end

-- Create a taglist widget
local function taglist(s)
  s.mytaglist = awful.widget.taglist{
    screen = s,
    filter = awful.widget.taglist.filter.all,
    buttons = gears.table.join(
      awful.button({}, 1, function(t) t:view_only() end),
      awful.button({modkey}, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
      awful.button({}, 3, awful.tag.viewtoggle),
      awful.button({modkey}, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
      awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
      awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
    )
  }
end

local function tasklist(s)
  -- Create a tasklist widget
  s.mytasklist = awful.widget.tasklist{
    screen = s,
    filter = awful.widget.tasklist.filter.currenttags,
    buttons = gears.table.join(
      awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal("request::activate", "tasklist", {raise = true})
        end
      end),
      awful.button({}, 3, function() awful.menu.client_list({theme = {width = 250}}) end),
      awful.button({}, 4, function() awful.client.focus.byidx(1) end),
      awful.button({}, 5, function() awful.client.focus.byidx(-1) end)
    )
  }
end

-- Create the top bar
local function topbar(s)
  s.mywibox = awful.wibar({position = "top", screen = s})
  layouts_selector(s)
  tasklist(s)

  -- Add widgets to the top bar
  s.mywibox:setup{
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      awful.util.mylauncher
    },
    s.mytasklist, -- Middle widgets
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      arrow(beautiful.bg_normal, beautiful.topbar_volume_bg),
      wibox.container.background(widgets.volume, beautiful.topbar_volume_bg),
      arrow(beautiful.topbar_volume_bg, beautiful.topbar_weather_bg),
      wibox.container.background(widgets.weather, beautiful.topbar_weather_bg),
      arrow(beautiful.topbar_weather_bg, beautiful.topbar_task_bg),
      wibox.container.background(widgets.task, beautiful.topbar_task_bg),
      arrow(beautiful.topbar_task_bg, beautiful.topbar_clock_bg),
      wibox.container.background(widgets.clock, beautiful.topbar_clock_bg),
      arrow(beautiful.topbar_clock_bg, "alpha"),
      s.layouts_selector
    }
  }
end


-- Create the bottombar
local function bottombar(s)
  s.bottombar = awful.wibar({position = "bottom", screen = s})
  taglist(s)

  -- Add widgets to the bottombar
  s.bottombar:setup{
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      s.mytaglist
    },
    -- Center widgets (empty)
    wibox.widget{nil},
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      arrow(beautiful.bg_normal, beautiful.bottombar_temp_bg),
      wibox.container.background(widgets.temperature, beautiful.bottombar_temp_bg),
      arrow(beautiful.bottombar_temp_bg, beautiful.bottombar_cpu_bg),
      wibox.container.background(widgets.cpu, beautiful.bottombar_cpu_bg),
      arrow(beautiful.bottombar_cpu_bg, beautiful.bottombar_mem_bg),
      wibox.container.background(widgets.memory, beautiful.bottombar_mem_bg),
      arrow(beautiful.bottombar_mem_bg, beautiful.bottombar_network_bg),
      wibox.container.background(widgets.network, beautiful.bottombar_network_bg)
    }
  }
end

return {
  top = topbar,
  bottom = bottombar
}
-- }}}
