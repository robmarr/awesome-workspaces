local beautiful = require("beautiful")
local lain = require("lain")
local naughty = require("naughty")

-- ALSA volume control
local function volume_set(command)
  return function ()
    os.execute(string.format(command, beautiful.volume.channel))
    beautiful.volume.update()
  end
end

local function volume_mute()
  os.execute(string.format("amixer -q set %s toggle", beautiful.volume.togglechannel or beautiful.volume.channel))
  beautiful.volume.update()
end

-- music player control
local function mpc_exec(command)
  return function()
    os.execute(string.format("mpc %s", command))
    beautiful.mpd.update()
  end
end

local function mpc_widget_on_off()
  local common = { text = "MPD widget ", position = "top_middle", timeout = 2 }
  if beautiful.mpd.timer.started then
      beautiful.mpd.timer:stop()
      common.text = common.text .. lain.util.markup.bold("OFF")
  else
      beautiful.mpd.timer:start()
      common.text = common.text .. lain.util.markup.bold("ON")
  end
  naughty.notify(common)
end

return {
  volume = {
    up = volume_set("amixer -q set %s 1%%+"),
    down = volume_set("amixer -q set %s 1%%-"),
    mute = volume_mute,
    min = volume_set("amixer -q set %s 0%%"),
    max = volume_set("amixer -q set %s 100%%")
  },
  player = {
    widget_toggle = mpc_widget_on_off,
    toggle = mpc_exec("toggle"),
    stop = mpc_exec("stop"),
    prev = mpc_exec("prev"),
    next = mpc_exec("next")
  }
}
